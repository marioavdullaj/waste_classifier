from __future__ import print_function
import numpy as np
import warnings
import tensorflow as tf
from keras.models import Model, Sequential
from keras.layers import Flatten, Dense, Input
from keras.layers import Convolution2D, MaxPooling2D
from keras.preprocessing import image
from keras.utils.layer_utils import convert_all_kernels_in_model
from keras.utils.data_utils import get_file
from keras import optimizers, backend as K
from imagenet_utils import preprocess_input, percentage
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import ModelCheckpoint, EarlyStopping
from keras.applications.vgg19 import VGG19
from keras.applications.inception_v3 import InceptionV3
from keras.applications.mobilenet_v2 import MobileNetV2
from sklearn.svm import SVC, LinearSVC
import os
from os import listdir
from os.path import isfile, join

# DATASET PATHs
DATASET_PATH_TRAIN = './data/training_data'
DATASET_PATH_VALID = './data/validation_data'
DATASET_PATH_TEST = './data/validation_data'
# WEIGHTS PATHs
VGG19_WEIGHTS_PATH = 'weights/vgg19_weights_tf_dim_ordering_tf_kernels.h5'
INCEPTION_WEIGHTS_PATH = 'weights/inception_v3_weights_tf_dim_ordering_tf_kernels.h5'
MOBILENETv2_WEIGHTS_PATH = 'weights/mobilenet_v2_weights_tf_dim_ordering_tf_kernels_1.0_224.h5'

# This method scans all the images contained in the indicated paths, dividing them in classes
# corresponding the name of folder they're contained in
def get_dataset(set = 'train'):
    print("DATASET ELABORATION...")
    DATASET_PATH = ''
    if(set == 'validation'):
        DATASET_PATH = DATASET_PATH_VALID
    elif(set == 'test'):
        DATASET_PATH = DATASET_PATH_TEST
    elif(set == 'train'):
        DATASET_PATH = DATASET_PATH_TRAIN
    else:
        return [],[]
    dirs = [d for d in os.listdir(DATASET_PATH) if not isfile(join(DATASET_PATH, d))]
    train_X = []
    train_Y = []
    for d in dirs:
        for root, dirs, files in os.walk(DATASET_PATH+'/'+d): 
            for f in files:
                if(f != '.DS_Store'):
                    img = image.load_img(DATASET_PATH+'/'+d+'/'+f, target_size=(224, 224))
                    x = image.img_to_array(img)
                    x = np.expand_dims(x, axis=0)
                    x /= 255.0
                    #x = preprocess_input(x)
                    train_X.append(x)
                    train_Y.append(d)
    print("DONE!")
    indx = np.arange(np.shape(train_X)[0])
    np.random.shuffle(indx)
    X = np.take(train_X, indx, axis = 0)
    Y = np.take(train_Y, indx, axis = 0)
    return X,Y

# This class creates the pre-trained CNN model indicated from the paramenter of the constructor
# loads the corresponding weights and sets the layers of the model not-trainable
# the get() method returns a Keras object corresponding the latter model
class CNN:
    model = None

    def init_model(self, cnn_type = None, trainable=False):
        if(cnn_type == None):
            return cnn_type

        cnn_model = None
        if(cnn_type == 'vgg19'):
            print("Using VGG19 net")
            cnn_model = VGG19(include_top=True, weights=None, input_tensor=Input(shape=(224,224,3)))

            print("LOADING WEIGHTS...")
            cnn_model.load_weights(VGG19_WEIGHTS_PATH)
            print("DONE!")
        if(cnn_type == 'incv3'):
            print("Using Inceptionv3 net")
            cnn_model = InceptionV3(include_top=True, weights=None, input_tensor=Input(shape=(224,224,3)))

            print("LOADING WEIGHTS")
            cnn_model.load_weights(INCEPTION_WEIGHTS_PATH)
            print("DONE!")
        if(cnn_type == 'mobilenet'):
            print("Using MobileNetv2 net")
            cnn_model = MobileNetV2(include_top=True, weights=None, input_tensor=Input(shape=(224,224,3)))
            
            print("LOADING WEIGHTS")
            cnn_model.load_weights(MOBILENETv2_WEIGHTS_PATH)
            print("DONE!")

        if(not trainable):
            for layer in cnn_model.layers:
                layer.trainable = False

        model = Model(input=cnn_model.input, output=cnn_model.layers[-2].output)
        return model

    def __init__(self, model = None, trainable=False):
        self.model = self.init_model(model, trainable)
    
    def get(self):
        return self.model

# This class receives as constructor parameter the type of pre-trained CNN
# It uses the CNN class to instantiate it and use it during the train() method.
# train() returns both the CNN and the SVM trained model.
class SVM_waste:
    cnn_model = None
    cnn_type = None

    def __init__(self, cnn = 'vgg19'):
        self.cnn_model = CNN(model = cnn).get()
        self.cnn_type = cnn

    def train(self, path = DATASET_PATH_TRAIN):
        feature_X = []
        class_Y = []

        X,y = get_dataset()
        P = percentage(len(X))

        svm = LinearSVC(C=0.001, penalty='l2')
        #svm = SVC(kernel='linear')
        # Feature extraction
        print("Feature exctraction using "+self.cnn_type+"..")
        for x in X: 
            feature_X.append(self.cnn_model.predict(x)[0])
            P.print_next()
        for _y in y:
            class_Y.append(_y)
        print("Extraction completed!")

        print("Training the SVM classifier...")
        svm.fit(feature_X, class_Y)
        print("Training complete!")

        print("Accuracy of the model over the training set: "+str(svm.score(feature_X, class_Y)))
        return (svm, self.cnn_model)

class DENSE_waste:
    cnn_model = None
    cnn_type = None
    final_model = None
    classes = ['cardboard', 'glass', 'metal', 'paper', 'plastic', 'umido']

    def __init__(self, cnn = 'vgg19', classes = 6, hidden_layers = 0, hidden_layers_size = 128):
        self.cnn_model = CNN(model = cnn).get()
        self.cnn_type = cnn

        model = Sequential()
        model.add(self.cnn_model)

        # Creating a dense layer with 6 classes with softmax activation function
        # it receives as input the output layer of the cnn model selected 
        # without the last layer, which has been removed for making it act as a feature extractor
        for i in range(hidden_layers):
            x = Dense(hidden_layers_size, activation = 'relu', name = 'hidden_layer_'+str(i+1))
            model.add(x)

        x = Dense(classes, activation='softmax', name='predictions')
        model.add(x)
        self.final_model = model
        #self.final_model = Model(input=self.cnn_model.input, output=x)

    def get(self, optimizer_type = 'adam'):
        # COMPILATION OF THE FINAL NET
        LEARNING_RATE = 0.01
        MOMENTUM = 0.9
        
        opt = None
        if(optimizer_type == 'sgd'):
            opt = optimizers.SGD(lr=LEARNING_RATE, momentum=MOMENTUM)
        if(optimizer_type == 'adam'):
            opt = optimizers.Adam(lr=LEARNING_RATE, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
        
        if(opt == None):
            print("No optimizer selected")
            return

        self.final_model.compile( loss = "categorical_crossentropy", optimizer = opt, metrics=["accuracy"] )

        print(self.final_model.summary())

        return self.final_model

    def train(self, EPOCHS = 10):
        #  Dataset of over 2500 classified waste images. The images are 512x384x3, which will be then rescaled to 224x224x3
        img_height = 224
        img_width = 224

        model = self.get()
        # Initiate the train and test generators with data Augumentation 
        train_datagen = ImageDataGenerator(
        rescale = 1./255,
        horizontal_flip = True,
        fill_mode = "nearest",
        zoom_range = 0.3,
        width_shift_range = 0.3,
        height_shift_range=0.3,
        rotation_range=30)

        test_datagen = ImageDataGenerator(
        rescale = 1./255,
        horizontal_flip = True,
        fill_mode = "nearest",
        zoom_range = 0.3,
        width_shift_range = 0.3,
        height_shift_range=0.3,
        rotation_range=30)

        train_generator = train_datagen.flow_from_directory(
        DATASET_PATH_TRAIN,
        target_size = (img_height, img_width),
        batch_size = 32, 
        class_mode = "categorical")

        validation_generator = test_datagen.flow_from_directory(
        DATASET_PATH_VALID,
        target_size = (img_height, img_width),
        class_mode = "categorical")

        # Save the model according to the conditions  
        checkpoint = ModelCheckpoint("weights/checkpoint/"+self.cnn_type+"_checkpoint.h5", monitor='val_acc', verbose=1, save_best_only=True, save_weights_only=False, mode='auto', period=1)
        early = EarlyStopping(monitor='val_acc', min_delta=0, patience=10, verbose=1, mode='auto')

        # Train the model 
        model.fit_generator(
        train_generator,
        steps_per_epoch = len(train_generator),
        epochs = EPOCHS,
        validation_data = validation_generator,
        validation_steps = len(validation_generator),
        callbacks = [checkpoint, early])

        return (model, self.cnn_model)
    
    # Ad-hoc evaluation method created for performing a binary accuracy of the predicted labels
    # and the true labels, both parameters of this method.
    def evaluate(self, model_final, preds, labels):
        if(len(labels) != len(preds)):
            print("labels and predictions do not have the same length")
            return

        loss = 0
        i = 0
        for pred in preds:
            indices = np.argsort(pred)
            indices = indices[0]
            pred.sort()
            pred = pred[::-1]
            indices = indices[::-1]

            predicted = self.classes[indices[0]]
            if(predicted != labels[i]):
                loss += 1
            i+=1
        
        acc = 1 - (loss / len(labels))*1.0
        return acc