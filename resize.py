import os
import glob
import numpy as np
from scipy import misc, ndimage
import cv2
import random 
from os import listdir
from os.path import isfile, join
import numpy

#Class percentage used to show the percentage of the process
class percentage:
    def __init__(self, total):
        self.p = 0
        self.act = 0
        self.total = total
    
    def print_next(self):
        perc = (self.act / self.total) * 100
        if (self.p * 10 <= perc):
            print( str( np.round(self.act / self.total * 100) )+'%' )
            self.p += 1
        self.act += 1

#Load images located in the PATH in the images and their filenames in filenames list 
#and contemporary delete the loaded images 
def load_images(PATH,images,P):
	for filename in os.listdir(PATH):
		img = cv2.imread(os.path.join(PATH,filename)) 
		# resize image
		resized = cv2.resize(img, (224, 224), interpolation = cv2.INTER_AREA)
		#filenames.append(filename)
		images.append(resized)
		os.remove(os.path.join(PATH,filename))
		P.print_next()

#For the label take the training and validation images, shuffle,
# and put 3/4 of them in the training set and 1/4 in the validation 
def shuffle_images(label):
	
	print("LOADING IMAGES: " + label)
	print("-----------------------------")

	TRAIN_PATH = './data\\training_data\\'+label+'\\' 
	VAL_PATH = './data\\validation_data\\'+label+'\\'
	NEW_PATH  = './dataset_original\\'

	images = []
	#filenames = []

	number_photos_train = len([name for name in os.listdir(TRAIN_PATH) if os.path.isfile(os.path.join(TRAIN_PATH, name))])
	number_photos_val = len([name for name in os.listdir(VAL_PATH) if os.path.isfile(os.path.join(VAL_PATH, name))])
	number_photos_new = len([name for name in os.listdir(NEW_PATH) if os.path.isfile(os.path.join(NEW_PATH, name))])
	number_photos = number_photos_train+number_photos_val+number_photos_new
	P = percentage(number_photos)

	load_images(TRAIN_PATH,images,P)
	load_images(VAL_PATH,images,P)
	load_images(NEW_PATH,images,P)

	print("LOADED IMAGES: " + str(number_photos_new))
	print("----------------------------------")
	print("--Start shuffling files: "+ label)
	print("----------------------------------")

	indx = np.arange(np.shape(images)[0])
	np.random.shuffle(indx)

	shuffled_images = np.take(np.array(images), indx, axis = 0)
	#shuffled_filenames = np.take(filenames, indx, axis = 0)

	#number_images = len(shuffled_images)
	P = percentage(number_photos)
	k = 0
	for image in shuffled_images:
		k = k + 1
		if k <= (3/4)*number_photos:
			cv2.imwrite(os.path.join(TRAIN_PATH, label+str(k)+".jpg"),image)
		else:
			cv2.imwrite(os.path.join(VAL_PATH, label+str(k)+".jpg"),image)
		P.print_next()

#Increase the dataset for a specific class
def main():
	print("--------------------------------")
	print("--Starting Dataset increasing --")
	print("--------------------------------")
	
	label = input('Inserire la classe: [cardboard][glass][metal][paper][plastic][umido]\n')
	shuffle_images(label)
	
	print("---------------------")
	print("--Dataset increased--")
	print("---------------------")

if __name__ == '__main__':
    main()