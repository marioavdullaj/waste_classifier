from __future__ import print_function
import numpy as np
import warnings
import tensorflow as tf
from keras.preprocessing import image
from keras import optimizers
from imagenet_utils import preprocess_input, percentage
from models import CNN, DENSE_waste, SVM_waste, get_dataset
import pickle
from sys import argv, exit

from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt

# method used for printing the output probabilities of the DENSE classifier
# which received as input parameter a single image to be classified
def print_preds(preds):
    if(preds == None):
        print("Error in function print_preds: missing preds argument")
        return
    classes = ['cardboard', 'glass', 'metal', 'paper', 'plastic', 'umido']
    preds = preds[0]

    indices = np.argsort(preds)
    indices = indices[0]
    preds.sort()
    preds = preds[::-1]
    indices = indices[::-1]
    preds = preds[0]
    
    for i in range(len(preds)):
        print(classes[indices[i]]+' with probability '+str(np.around(a=preds[i]*100, decimals=2))+'%')

# Calculation of the single class accuracies
def single_class_accuracy(class_name, true_labels, preds):
    if(class_name == None):
        print("Error in single_class_accuracy: missing one or more arguments")
        return
    
    correct = 0
    class_size = 0
    for i in range(len(true_labels)):
        if(true_labels[i] == class_name):
            class_size += 1
            if(true_labels[i] == preds[i]):
                correct += 1
    return 1.0*correct/class_size
    

if __name__ == '__main__':
    if(len(argv) < 3):
        print("python classify.py TOP_LAYER=[dense, svm] CNN_type=[vgg19, incv3, mobilenet] [PATH_TO_IMAGE]")
        exit()

    input_image = False
    # If we pass a single image to be classified, we wanna rescale and normalize its pixels
    if(len(argv) == 4):
        input_image = True
        img_path = argv[3]
        img = image.load_img(img_path, target_size=(224, 224))
        x = image.img_to_array(img)
        x = np.expand_dims(x, axis=0)
        x /= 255.0
        #x = preprocess_input(x)
        print('Input image shape:', x.shape)
        print(type(x))

    TOP_LAYER = argv[1]
    cnn_type = argv[2]

    if(TOP_LAYER == "dense"):
        HIDDEN_LAYER_SIZE = 128
        HIDDEN_LAYER = 0
        CLASSES = 6

        dense = DENSE_waste(cnn_type, classes=CLASSES, hidden_layers=HIDDEN_LAYER, hidden_layers_size=HIDDEN_LAYER_SIZE)
        model_final = dense.get()
        
        print("LOADING THE MODEL WEIGHTS...")
        W_PATH = 'weights/'+cnn_type+'_waste_classifier_weights.h5'
        model_final.load_weights(W_PATH)
        print("DONE!")

        feature_X = []
        labels = []
        if(not input_image):
            X,y = get_dataset('test')
            for x in X[:]: 
                feature_X.append(x)
            for _y in y[:]:
                labels.append(_y)
        else:
            feature_X.append(x)

        print("Classifing...")

        P = percentage(len(feature_X))
        preds = []
        for x in feature_X:
            preds.append(model_final.predict(x))
            P.print_next()
        
        if(input_image):
            print_preds(preds)
        if(not input_image):
            print("ACCURACY SCORE (on validation set): ")
            print(dense.evaluate(model_final, preds, labels))


    if(TOP_LAYER == "svm"):
        cnn_model = CNN(cnn_type).get()
        cnn_model.summary()
        model_final = pickle.load(open('weights/'+cnn_type+'_svm_weights.sav', 'rb'))
        feature_X = []
        labels = []
        if(not input_image):
            X,y = get_dataset('test')
            P = percentage(len(X))
            print("Extracting and classifing "+str(np.shape(X)[0])+" waste images")
            for x in X[:]: 
                feature_X.append(cnn_model.predict(x)[0])
                P.print_next()
            for _y in y[:]:
                labels.append(_y)
        else:
            feature_X.append(cnn_model.predict(x)[0])
        print("Classifing...")

        preds = model_final.predict(feature_X)

        if(not input_image):
            print("FINAL SCORE: "+str(model_final.score(feature_X, labels)))
        else:
            print(preds[0])

        if(not input_image):
            # CONFUSION MATRIX
            unique_classes = np.unique(labels)
            classes = []
            for u in unique_classes:
                classes.append(u)
            cm = confusion_matrix(labels, preds, unique_classes)
            print(cm)
            fig = plt.figure()
            ax = fig.add_subplot(111)
            cax = ax.matshow(cm)
            plt.title('Confusion matrix of the classifier')
            fig.colorbar(cax)
            plt.xlabel('Predicted')
            plt.ylabel('True')
            ax.set_xticklabels(['']+classes)
            ax.set_yticklabels(['']+classes)
            plt.show()

        # SINGLE CLASS ACCURACY
        single_accuracies = []
        for c in unique_classes:
            accur = single_class_accuracy(c, labels, preds)
            single_accuracies.append(str(c)+": "+str(accur))

        print(single_accuracies)

