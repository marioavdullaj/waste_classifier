from sklearn_porter import Porter
import pickle
from sys import argv, exit
import numpy as np

if(len(argv) != 2):
    print("sklearn-porting.py [ vgg19 | incv3 | mobilenet ]")
    exit()
cnn_type = argv[1]
model = pickle.load(open('weights/'+cnn_type+'_svm_weights.sav', 'rb'))
porter = Porter(model, language='java')
output = porter.export(embed_data=True)

f = open("./java_files/SVC_"+cnn_type+".java","w")
f.write(output)
f.close()