from models import DENSE_waste
from sys import argv, exit
import tensorflow as tf

if(len(argv) < 2):
    print("train_dense.py [ vgg19 | incv3 | mobilenet ]")
    exit()

cnn_type = None
if(argv[1] == 'vgg19' or argv[1] == 'incv3' or argv[1] == 'mobilenet'):
    cnn_type = argv[1]
else:
    print("train_dense.py [ vgg19 | incv3 | mobilenet ]")
    exit()

# SAVE PATH
W_PATH = 'weights/'+cnn_type+'_waste_classifier_weights.h5'

model = DENSE_waste(cnn_type, classes = 6, hidden_layers= 0, hidden_layers_size=64)
(final_model, cnn_model) = model.train( EPOCHS=5 )
final_model.save_weights(W_PATH)