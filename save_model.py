from models import CNN
from sys import argv, exit
from keras import backend as K
import tensorflow as tf
from tensorflow.python.framework.graph_util import convert_variables_to_constants

if(len(argv) != 2):
    print("save_model.py [ vgg19 | incv3 | mobilenet ]")
    exit()
cnn_type = argv[1]fr
print("SAVING THE MODEL INTO HDF5 FORMAT")
model = CNN(cnn_type).get()
model.save('./java_files/models/'+cnn_type+'_model.h5')

from tensorflow.contrib import lite


print("CONVERTING HDF5 MODEL INTO TFLITE MODEL")
converter = lite.TFLiteConverter.from_keras_model_file('./java_files/models/'+cnn_type+'_model.h5')
tfmodel = converter.convert()
open("./java_files/models/"+cnn_type+"_model.tflite" , "wb").write(tfmodel)