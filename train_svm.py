from models import SVM_waste
from sys import argv, exit
import tensorflow as tf
import pickle

from sklearn_porter import Porter

if(len(argv) < 2):
    print("train_svm.py [ vgg19 | incv3 | mobilenet ]")
    exit()

cnn_type = None
if(argv[1] == 'vgg19' or argv[1] == 'incv3' or argv[1] == 'mobilenet'):
    cnn_type = argv[1]
else:
    print("train_svm.py [ vgg19 | incv3 | mobilenet ]")
    exit()

FILENAME = cnn_type+'_svm_weights.sav'
DIR_PATH = 'weights/'

model = SVM_waste(cnn=cnn_type)
(svm, cnn_model) = model.train()
pickle.dump(svm, open(DIR_PATH+FILENAME, 'wb'))

