import glob
import os
import cv2 
import numpy as np
from scipy import misc, ndimage
import random 
from os import listdir
from os.path import isfile, join
import numpy
import cv2

#Class percentage used to show the percentage of the process
class percentage:
    def __init__(self, total):
        self.p = 0
        self.act = 0
        self.total = total
    
    def print_next(self):
        perc = (self.act / self.total) * 100
        if (self.p * 10 <= perc):
            print( str( np.round(self.act / self.total * 100) )+'%' )
            self.p += 1
        self.act += 1

#Load images located in the PATH in the images and their filenames in filenames list 
#and contemporary delete the loaded images 
def load_images(PATH,images,filenames,P):
    for filename in os.listdir(PATH):
        img = cv2.imread(os.path.join(PATH,filename))
        filenames.append(filename)
        images.append(img)
        os.remove(os.path.join(PATH,filename))
        P.print_next()

#For the label take the training and validation images, shuffle,
# and put 3/4 of them in the training set and 1/4 in the validation 
def shuffle_images(label):
        
    print("LOADING IMAGES: " + label)
    print("-----------------------------")
    TRAIN_PATH = './data\\training_data\\'+label+'\\' 
    VAL_PATH = './data\\validation_data\\'+label+'\\'

    images = []
    filenames = []
    number_photos_train = len([name for name in os.listdir(TRAIN_PATH) if os.path.isfile(os.path.join(TRAIN_PATH, name))])
    number_photos_val = len([name for name in os.listdir(VAL_PATH) if os.path.isfile(os.path.join(VAL_PATH, name))])
    number_photos = number_photos_train+number_photos_val
    P = percentage(number_photos)
    load_images(TRAIN_PATH,images,filenames,P)
    load_images(VAL_PATH,images,filenames,P)


    print("LOADED IMAGES: " + label)
    print("----------------------------------")
    print("--Start shuffling files: "+ label)
    print("----------------------------------")

    indx = np.arange(np.shape(images)[0])
    np.random.shuffle(indx)

    shuffled_images = np.take(np.array(images), indx, axis = 0)
    shuffled_filenames = np.take(filenames, indx, axis = 0)
    
    number_images = len(shuffled_images)
    P = percentage(number_images)
    k = 0
    for image in shuffled_images:
        k = k + 1
        if k <= (3/4)*number_images:
            cv2.imwrite(os.path.join(TRAIN_PATH, label+str(k)+".jpg"),image)
        else:
            cv2.imwrite(os.path.join(VAL_PATH, label+str(k)+".jpg"),image)
        P.print_next()

    print("Start shuffling files: "+ label)
    print("-----------------------------")

#Used for shuffle all images and rename all correctly
#N.B. : DON'T INTERRUPT THE EXECUTION BECAUSE SOME FILE CAN BE DELETED!!!!!!
def main():
    shuffle_images('cardboard')
    shuffle_images('glass')
    shuffle_images('paper')
    shuffle_images('metal')
    shuffle_images('plastic')
    shuffle_images('umido')

  
if __name__== "__main__":
  main()

